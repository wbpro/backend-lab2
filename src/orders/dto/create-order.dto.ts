import { IsNotEmpty, IsPositive } from 'class-validator';
class CreateOrderItemsDto {
  @IsNotEmpty()
  @IsPositive()
  productId: number;

  @IsNotEmpty()
  @IsPositive()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  @IsPositive()
  customerId: number;

  @IsNotEmpty()
  orderItems: CreateOrderItemsDto[];
}
