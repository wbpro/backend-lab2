import { IsNotEmpty, Length, IsPositive } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 50)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @Length(1, 1)
  gender: string;

  @IsNotEmpty()
  @Length(10, 10)
  tel: string;
}
