import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm/dist';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private useProductsRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto) {
    return this.useProductsRepository.save(createProductDto);
  }

  findAll() {
    return this.useProductsRepository.find();
  }

  findOne(id: number) {
    return this.useProductsRepository.findOneBy({ id: id });
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    return this.useProductsRepository.update(id, updateProductDto);
  }

  async remove(id: number) {
    const product = await this.useProductsRepository.findOneBy({ id: id });
    return this.useProductsRepository.softRemove(product);
  }
}
